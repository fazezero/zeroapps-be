'use strict';

var utils = require('../utils/writer.js');
var Statistics = require('../service/StatisticsService');

module.exports.statsGET = function statsGET (req, res, next) {

  // console.log('request coming from Stat \n');
  // console.log(req);
  // console.log('request coming from Stat \n');

  Statistics.statsGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
