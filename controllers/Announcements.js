'use strict';

var utils = require('../utils/writer.js');
var Announcements = require('../service/AnnouncementsService');

module.exports.announcementGET = function announcementGET (req, res, next) {

  Announcements.announcementGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
