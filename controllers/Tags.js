'use strict';

var utils = require('../utils/writer.js');
var Tags = require('../service/TagsService');

module.exports.tagsGET = function tagsGET (req, res, next) {

  var query = req.swagger.params['query'].value;
  var exclude = req.swagger.params['exclude'].value;

  Tags.tagsGET(query,exclude)
  .then(function (response) {

    console.log('got an array of objects');
    console.log(response);

    console.log(response.tag);

    var myResponse = response.tag.split(",");
    utils.writeJson(res, myResponse);

  })
  .catch(function (response) {
    utils.writeJson(res, response);

  });


  // Tags.tagsGET(query,exclude)
  //   .then(function (response) {
  //     utils.writeJson(res, response);
  //   })
  //   .catch(function (response) {
  //     utils.writeJson(res, response);
  //   });
};
