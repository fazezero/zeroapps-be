'use strict';

var utils = require('../utils/writer.js');
var DApps = require('../service/DAppsService');

module.exports.dappsGET = function dappsGET (req, res, next) {

  var category = req.swagger.params['category'].value;
  var refine = req.swagger.params['refine'].value;
  var tags = req.swagger.params['tags'].value;
  var text = req.swagger.params['text'].value;
  var limit = req.swagger.params['limit'].value;
  var offset = req.swagger.params['offset'].value;


  DApps.dappsGET(category,refine,tags,text,limit,offset)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.dappsPOST = function dappsPOST (req, res, next) {
  var body = req.swagger.params['body'].value;
  console.log(body);

  DApps.dappsPOST(body)
    .then(function (body) {
      utils.writeJson(res, body);
    })
    .catch(function (body) {
      utils.writeJson(res, body);
    });
};

module.exports.dappsSlugGET = function dappsSlugGET (req, res, next) {
  var slug = req.swagger.params['slug'].value;
  DApps.dappsSlugGET(slug)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
