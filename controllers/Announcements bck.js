'use strict';

var utils = require('../utils/writer.js');
var Announcements = require('../service/AnnouncementsService');

module.exports.announcementGET = function announcementGET (req, res, next) {

  // console.log('request coming from Announcements \n');
  // console.log('lll');
  // console.log('request coming from Announcements \n');
  // console.log(res);

  Announcements.announcementGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
