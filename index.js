'use strict';

var fs = require('fs'),
    path = require('path'),
    http = require('http');

var app = require('connect')();
var swaggerTools = require('swagger-tools');
var jsyaml = require('js-yaml');
var mongoose = require ('mongoose');



// // ------------=====================------======----====---==--==--==-
//   //Testing : https://github.com/apigee-127/swagger-tools/issues/108
//
// var SwaggerConnect = require('swagger-connect');
// module.exports = app;
//
// // ------------=====================------======----====---==--==--==-
//   //Testing : https://github.com/apigee-127/swagger-tools/issues/108


var serverPort = 8080;

mongoose.connect('mongodb://appsadmin:appsadmin123@ds131826.mlab.com:31826/zero-apps');


app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});


// swaggerRouter configuration
var options = {
  swaggerUi: path.join(__dirname, '/swagger.json'),
  controllers: path.join(__dirname, './controllers'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname,'api/swagger.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());


  // Validate Swagger requests
  app.use(middleware.swaggerValidator());


  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));


  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());



  // ------------=====================------======----====---==--==--==-
  //                      FOR TESTING (NOTE WORKING)
  //Testing : https://github.com/apigee-127/swagger-tools/issues/108
  //
  // var config = {
  //   appRoot: __dirname // required config
  // };

  // SwaggerConnect.create(config, function(err, swaggerConnect) {
  //   if (err) { throw err; }

  //   // install middleware
  //   swaggerConnect.register(app);

  //   // Custom error handler that returns JSON
  //   app.use(function(err, req, res, next) {
  //     if (typeof err !== 'object') {
  //       // If the object is not an Error, create a representation that appears to be
  //       err = {
  //         message: String(err) // Coerce to string
  //       };
  //     } else {
  //       // Ensure that err.message is enumerable (It is not by default)
  //       Object.defineProperty(err, 'message', { enumerable: true });
  //     }

  //     // Return a JSON representation of #/definitions/ErrorResponse
  //     res.setHeader('Content-Type', 'application/json');
  //     res.end(JSON.stringify(err));
  //   });

  //   var port = process.env.PORT || 10010;
  //   app.listen(port);

  //   console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  // });
  //
  // ------------=====================------======----====---==--==--==-



  // Start the server
  http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
    console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);

  });

});
