'use strict';


/**
 * Retrieve Statistics
 *
 *
 * returns inline_response_200_3
 **/


var mongoose = require('mongoose');

var DApps = require('../models/dapp');



exports.statsGET = function() {

  return new Promise(function(resolve, reject) {

    var examples = {};

    examples['application/json'] = {
     "dappCount" : 750
    };

    DApps.find()
    .exec(function (err, dapps) {
      console.log('count ----- :', dapps.length);
      var result = { "dappCount" : dapps.length};
      if (Object.keys(result).length > 0) {
        //console.log('examples----', result);
        resolve(result);
      } else {
          resolve();
      }
    });
  });
}

