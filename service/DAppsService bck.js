'use strict';


/**
 * Retrieve DApps
 *
 *
 * category String Shows top results of a category  : Default: `recently added` (optional)
 * refine String Filters DApps by status  : Default: `any` (optional)
 * tags String List of tags retrieved using `AND` operator. We use `AND` to refine the results as the user adds tags. (optional)
 * text String Text query which will return DApps (optional)
 * limit String The maximum number of items to return. Default 50. (optional)
 * offset String Where to start in the list of items. Default 0. (optional)
 * returns inline_response_200_1
 **/
exports.dappsGET = function(category,refine,tags,text,limit,offset) {

  return new Promise(function(resolve, reject) {

    var examples = {};

    examples['application/json'] =
    [
      {
        "author" : "Julian Zawistowski",
        "additionalAuthors" : "Piotr ‘Viggith’ Janiuk, Andrzej Regulski, Aleksandra Skrzypczak",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "Golem",
        "slug" : "golem",
        "status" : "prototype",
        "teaser" : "Distributed computation."
      }, {
        "author" : "Mona El Isa",
        "additionalAuthors" : "Reto Trinkler",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "Melonport",
        "slug" : "melonport",
        "status" : "wip",
        "teaser" : "A portal to asset management."
      }, {
        "author" : "Toby Hoenisch",
        "additionalAuthors" : "Michael Sperk, Paul Kitti, Julian Hosp",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "TenX",
        "slug" : "tenx",
        "status" : "live",
        "teaser" : "Connecting your blockchain assets for everyday use."
      }, {
        "author" : "Luis Cuende",
        "additionalAuthors" : "Jorge Izquierdo",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "Aragon",
        "slug" : "aragon",
        "status" : "prototype",
        "teaser" : "Create and manage DAOs."
      }, {
        "author" : "Jarrad Hope",
        "additionalAuthors" : "Carl Bennetts",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "Status",
        "slug" : "status",
        "status" : "prototype",
        "teaser" : "An open source mobile client."
      }, {
        "author" : "Rodrigo Sainz",
        "additionalAuthors" : "Cristobal Pereira",
        "badges" : [ "complete", "token", "featured", "hot" ],
        "id" : "1229382",
        "isNew" : true,
        "name" : "Godzillion",
        "slug" : "godzillion",
        "status" : "live",
        "teaser" : "Marketplace where people vote, fund and trade startups."
      }
    ];


    if (Object.keys(examples).length > 0) {
      console.log('length is %d ----- ', Object.keys(examples).length);
      resolve(examples[Object.keys(examples)[0]]);
    }

    else {
      resolve();
     }

  });
}


/**
 * Create DApp
 *
 *
 * body Body  (optional)
 * no response value expected for this operation
 **/
exports.dappsPOST = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Retrieve DApp
 *
 *
 * slug String Unique slug
 * returns inline_response_200_2
 **/
exports.dappsSlugGET = function(slug) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] =
    {
      "author" : "Luis Cuende",
      "additionalAuthors" : "Joris, Fauve, Martin",
      "badges" : [ "complete", "token", "featured", "hot" ],
      "created" : "April 11, 1985",
      "description" : "Create unstoppable companies...",
      "isNew" : true,
      "lastUpdated" : "April 11, 1985",
      "license" : "MIT",
      "mainnet" : "0x1293873df786348763487634",
      "name" : "Aragon",
      "related" : [ {
        "name" : "Aragon",
        "slug" : "aragon",
        "status" : "live",
        "teaser" : "Curated collection of..."
        },
        {
        "name" : "Aragon",
        "slug" : "aragon",
        "status" : "live",
        "teaser" : "Curated collection of..."
        }
      ],
      "ropsten" : "0x1293873df786348763487634",
      "slug" : "aragon",
      "socials" : {
        "github" : {
          "url" : "https://github.com"
        },
        "twitter" : {
          "url" : "https://twitter.com"
        }
      },
      "status" : "live",
      "tags" : [ "game", "ico" ],
      "teaser" : "Create unstoppable companies",
      "website" : "https://google.com"
    };

    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

