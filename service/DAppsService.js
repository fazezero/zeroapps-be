
'use strict';

/**
 * Retrieve DApps
 *
 *
 * category String Shows top results of a category  : Default: `recently added` (optional)
 * refine String Filters DApps by status  : Default: `any` (optional)
 * tags String List of tags retrieved using `AND` operator. We use `AND` to refine the results as the user adds tags. (optional)
 * text String Text query which will return DApps (optional)
 * limit String The maximum number of items to return. Default 50. (optional)
 * offset String Where to start in the list of items. Default 0. (optional)
 * returns inline_response_200_1
 **/


var mongoose = require('mongoose');

var DApps = require('../models/dapp');


exports.dappsGET = function(category,refine,tags,text,limit,offset) {

  return new Promise(function(resolve, reject) {

    var dapps = {};

    // it is important here to name the input to exec as 'dapps' as this is the
    // name of the collection in MondoDB

    DApps.where("category", category)
    .exec(function (err, dapps) {
      // console.log(dapps);
      if (Object.keys(dapps).length > 0) {
        resolve(dapps);
        // resolve(dapps[Object.keys(dapps)[0]]);
      }
      else {
        resolve();
      }
    });

  });
}


/**
 * Create DApp
 *
 *
 * body Body  (optional)
 * no response value expected for this operation
 **/


exports.dappsPOST = function(input) {
  return new Promise(function(resolve, reject) {

  //mapping the MondoDB object to input that is in the form of {data : {...}}
    var dapps = new DApps({
      mainnet: input.data.mainnet,
      author: input.data.author,
      category: input.data.category,
      additionalAuthors: input.data.additionalAuthors,
      description: input.data.description,
      email: input.data.email,
      license: input.data.license,
      name: input.data.name,
      status: input.data.status,
      isNew: input.data.isNew,
      socials: input.data.socials,
      tags: input.data.tags,
      badges: input.data.badges,
      teaser: input.data.teaser,
      testnet: input.data.testnet,
      website: input.data.website
    });

    // it is important here to name the input to exec as 'dapps' as this is the
    // name of the collection in MondoDB

    // console.log(dapps);
    // console.log('hello')

    dapps.save(function (err, response) {

      // console.log(response);

      resolve('error', response);

    });
  });
}


/**
 * Retrieve DApp
 *
 *
 * slug String Unique slug
 * returns inline_response_200_2
 **/

exports.dappsSlugGET = function(slug) {
  return new Promise(function(resolve, reject) {


    var examples = {};
    examples['application/json'] =
    {
      "author" : "Luis Cuende",
      "additionalAuthors" : "Joris, Fauve, Martin",
      "badges" : [ "complete", "token", "featured", "hot" ],
      "created" : "April 11, 1985",
      "description" : "Create unstoppable companies...",
      "isNew" : true,
      "lastUpdated" : "April 11, 1985",
      "license" : "MIT",
      "mainnet" : "0x1293873df786348763487634",
      "name" : "Aragon",
      "related" : [ {
        "name" : "Aragon",
        "slug" : "aragon",
        "status" : "live",
        "teaser" : "Curated collection of..."
        },
        {
        "name" : "Aragon",
        "slug" : "aragon",
        "status" : "live",
        "teaser" : "Curated collection of..."
        }
      ],
      "ropsten" : "0x1293873df786348763487634",
      "slug" : "aragon",
      "socials" : {
        "github" : {
          "url" : "https://github.com"
        },
        "twitter" : {
          "url" : "https://twitter.com"
        }
      },
      "status" : "live",
      "tags" : [ "game", "ico" ],
      "teaser" : "Create unstoppable companies",
      "website" : "https://google.com"
    };

    var dapps = {};

    DApps.where("slug", slug)
    .exec(function (err, dapps) {
      console.log('returnec from slug --- ',dapps);

      var result = dapps[0];
      if (Object.keys(result).length > 0) {
        resolve(result);
        // resolve(dapps[Object.keys(dapps)[0]]);
      }
      else {
        resolve();
      }
    });


    // if (Object.keys(examples).length > 0) {
    //   resolve(examples[Object.keys(examples)[0]]);
    // } else {
    //   resolve();
    // }


  });
}

