'use strict';



/**
 * Retrieve Announcement
 *
 *
 * returns inline_response_200
 **/

exports.announcementGET = function() {

  return new Promise(function(resolve, reject) {


    var examples = {};

    examples['application/json'] = {
      "status" : true,
      "message" : "New website for State of the DApps",
      "url" : "https://www.fazezero.com/",
      "urlText" : "Check it out"
    };





    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    }

    else {
      resolve();
    }

  });
}

