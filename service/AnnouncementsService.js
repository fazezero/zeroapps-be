/**
 * Retrieve Announcement
 *
 *
 * returns inline_response_200
 **/

'use strict';
var mongoose = require('mongoose');

var Announcement = require('../models/announcement');


exports.announcementGET = function() {
  return new Promise(function(resolve, reject) {

    var annoucements = {};

    Announcement.find()
      .exec(function (err, announcements) {
        console.log(announcements);
        if (Object.keys(announcements).length > 0) {
          //resolve(announcements);
          resolve(announcements[Object.keys(announcements)[0]]);
        }
        else {
          resolve();
        }
      });

  });
}

