var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var mongooseUniqueValidator = require('mongoose-unique-validator');



var schema = new Schema({
  status  : { type : Boolean, required: true},
  message : { type : String, required: true },
  url     : { type : String, required: true /*, Description : `null` if empty */},
  urlText : { type : String, required: true /*, Description : `null` if empty */}
});


module.exports = mongoose.model('Announcement', schema);
