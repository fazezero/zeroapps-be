var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var mongooseUniqueValidator = require('mongoose-unique-validator');


var schema = new Schema({

    author : {
      type : String /*description : can't be empty */
    },
    category : {
      type : String
    },
    additionalAuthors : {
      type : String /* description : `null` if empty */
    },
    badges : {
      type : Object,
      properties : {
        complete : { type : String },
        token : { type : String },
        featured : { type : String },
        hot : { type : String }
      }
      //description : returns an array, `[]` if empty
    },
    created : {
      type : String
      //description : can't be empty
    },
    description : {
      type : String
      //description : can't be empty
    },
    isNeww: {
      type : Boolean
    },
    lastUpdated : {
      type : String
      //description : can't be empty
    },
    license : {
      type : String
      //description : can't be empty
    },
    mainnet : {
      type : String
      //description : `null` if empty
    },
    name : {
      type : String
      //description : can't be empty
    },
    related : {
      type : Object,
      properties : { name : { type : String /*description : can't be empty*/ },
                     slug : { type : String /*description : can't be empty*/ },
                   status : { type : String /*description : `live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`, `stealth`, `unknown` */},
                   teaser : { type : String /*description : can't be empty */},
      },
     /* required : [ name, slug, status, teaser ] */
     /* description : array, `[]` if empty */
    },
    ropsten : {
      type : String /* description : `null` if empty */
    },
    slug : {
      type : String  /* description : can't be empty */
    },
    logoUrl : {
      type : String
    },
    socials : {
      type : Object,
      properties : {
        facebook : {
          type : Object,
          properties : {
            url : {
              type : String /* description : can't be empty */
            }
          } /* required : [ url ] */
        },
        github : {
          type : Object,
          properties : {
            url : {
              type : String
              //description : can't be empty
            }
          } /*required : [ url ] */
        },
        medium : {
          type : Object,
          properties : {
            url : {
              type : String
              //description : can't be empty
            }
          } /* required : [ url ] */
        },
        other : {
          type : Object,
          properties : {
            url : {
              type : String /* description : can't be empty */
            }
          }, /* required : [ url ] */
        },
        reddit : {
          type : Object,
          properties : {
            url : {
              type : String /* description : can't be empty */
            }
          }, /* required : [ url ] */
        },
        slack : {
          type : Object,
          properties : {
            url : {
              type : String /* description : can't be empty */
            }
          }, /*   required : [ url ] */
        },
        twitter : {
          type : Object,
          properties : {
            url : {
              type : String /* description : can't be empty */
            }
          }, /* required : [ url ] */
        }
      }, /* description : `[]` if empty */
    },
    status : {
      type : String /* description : `live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`, `stealth`, `unknown` */
    },
    tags : {
      type : Object,
      properties : { }
      /* description : `[]` if empty */
    },
    teaser : {
      type : String
      //description : can't be empty
    },
    website : {
      type : String /* description : can't be empty */
    }
  //required : [ author, additionalAuthors, badges, created, description, isNew, lastUpdated, license, mainnet, name, related, ropsten, slug, socials, status, tags, teaser, website ]
});


module.exports = mongoose.model('Dapp', schema);



