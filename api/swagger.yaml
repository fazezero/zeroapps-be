---
swagger: "2.0"
info:
  description: "Welcome to the State of the DApps API doc. The Github org is at https://github.com/state-of-the-dapps,\
    \ and the live site is at https://www.fazezero.com"
  version: "1.0.0"
  title: "State of the DApps API v1"
paths:
  /announcement:
    get:
      tags:
      - "Announcements"
      summary: "Retrieve Announcement"
      description: ""
      operationId: "announcementGET"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "+ Results: min (1), max (1)"
          schema:
            $ref: "#/definitions/inline_response_200"
          examples:
            application/json:
              status: true
              message: "New website for State of the DApps"
              url: "https://www.fazezero.com/"
              urlText: "Check it out"
          headers: {}
        204:
          description: "No Content"
          examples: {}
          headers: {}
      x-swagger-router-controller: "Announcements"
  /dapps:
    get:
      tags:
      - "DApps"
      summary: "Retrieve DApps"
      description: ""
      operationId: "dappsGET"
      produces:
      - "application/json"
      parameters:
      - name: "category"
        in: "query"
        description: "Shows top results of a category\n\n: Default: `recently added`"
        required: false
        type: "string"
        enum:
        - "recently added"
        - "hot"
        - "most-viewed"
        - "recently updated"
        - "new"
      - name: "refine"
        in: "query"
        description: "Filters DApps by status\n\n: Default: `any`"
        required: false
        type: "string"
        enum:
        - "any"
        - "live"
        - "wip"
        - "concept"
        - "prototype"
        - "demo"
        - "abandoned"
        - "stealth"
        - "unknown"
      - name: "tags"
        in: "query"
        description: "List of tags retrieved using `AND` operator. We use `AND` to\
          \ refine the results as the user adds tags."
        required: false
        type: "string"
      - name: "text"
        in: "query"
        description: "Text query which will return DApps"
        required: false
        type: "string"
      - name: "limit"
        in: "query"
        description: "The maximum number of items to return. Default 50."
        required: false
        type: "string"
      - name: "offset"
        in: "query"
        description: "Where to start in the list of items. Default 0."
        required: false
        type: "string"
      responses:
        200:
          description: "+ Results: min (0), max (200)"
          schema:
            $ref: "#/definitions/inline_response_200_1"
          examples:
            application/json:
            - author: "Nasser Rahal"
              additionalAuthors: "Piotr ‘Viggith’ Janiuk, Andrzej Regulski, Aleksandra\
                \ Skrzypczak"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "Golem"
              slug: "golem"
              status: "prototype"
              teaser: "Distributed computation."
            - author: "Mona El Isa"
              additionalAuthors: "Reto Trinkler"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "Melonport"
              slug: "melonport"
              status: "wip"
              teaser: "A portal to asset management."
            - author: "Toby Hoenisch"
              additionalAuthors: "Jad Sperk, Paul Kitti, Julian Hosp"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "TenX"
              slug: "tenx"
              status: "live"
              teaser: "Connecting your blockchain assets for everyday use."
            - author: "Luis Cuende"
              additionalAuthors: "Jorge Izquierdo"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "Aragon"
              slug: "aragon"
              status: "prototype"
              teaser: "Create and manage DAOs."
            - author: "Jarrad Hope"
              additionalAuthors: "Carl Bennetts"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "Status"
              slug: "status"
              status: "prototype"
              teaser: "An open source mobile client."
            - author: "Rodrigo Sainz"
              additionalAuthors: "Cristobal Pereira"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              id: "1229382"
              isNew: true
              name: "Godzillion"
              slug: "godzillion"
              status: "live"
              teaser: "Marketplace where people vote, fund and trade startups."
          headers: {}
      x-swagger-router-controller: "DApps"
    post:
      tags:
      - "DApps"
      summary: "Create DApp"
      description: ""
      operationId: "dappsPOST"
      parameters:
      - in: "body"
        name: "body"
        required: false
        schema:
          $ref: "#/definitions/body"
      responses:
        201:
          description: "Created"
          examples: {}
          headers: {}
        401:
          description: "Unauthorized"
          examples: {}
          headers: {}
      x-swagger-router-controller: "DApps"
  /dapps/{slug}:
    get:
      tags:
      - "DApps"
      summary: "Retrieve DApp"
      description: ""
      operationId: "dappsSlugGET"
      produces:
      - "application/json"
      parameters:
      - name: "slug"
        in: "path"
        description: "Unique slug"
        required: true
        type: "string"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/inline_response_200_2"
          examples:
            application/json:
              author: "Luis Cuende"
              additionalAuthors: "Joris, Fauve, Martin"
              badges:
              - "complete"
              - "token"
              - "featured"
              - "hot"
              created: "April 11, 1985"
              description: "Create unstoppable companies..."
              isNew: true
              lastUpdated: "April 11, 1985"
              license: "MIT"
              mainnet: "0x1293873df786348763487634"
              name: "Aragon"
              related:
              - name: "Aragon"
                slug: "aragon"
                status: "live"
                teaser: "Curated collection of..."
              - name: "Aragon"
                slug: "aragon"
                status: "live"
                teaser: "Curated collection of..."
              ropsten: "0x1293873df786348763487634"
              slug: "aragon"
              socials:
                github:
                  url: "https://github.com"
                twitter:
                  url: "https://twitter.com"
              status: "live"
              tags:
              - "game"
              - "ico"
              teaser: "Create unstoppable companies"
              website: "https://google.com"
          headers: {}
        404:
          description: "Not Found"
          examples: {}
          headers: {}
      x-swagger-router-controller: "DApps"
  /tags:
    get:
      tags:
      - "Tags"
      summary: "Retrieve Tags"
      description: ""
      operationId: "tagsGET"
      produces:
      - "application/json"
      parameters:
      - name: "query"
        in: "query"
        description: "The search query"
        required: false
        type: "string"
      - name: "exclude"
        in: "query"
        description: "List of tags to exclude in results"
        required: false
        type: "string"
      responses:
        200:
          description: "+ Results: min (0), max (15)"
          examples:
            application/json:
            - "game"
            - "gambling"
          headers: {}
      x-swagger-router-controller: "Tags"
  /stats:
    get:
      tags:
      - "Statistics"
      summary: "Retrieve Statistics"
      description: ""
      operationId: "statsGET"
      produces:
      - "application/json"
      parameters: []
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/inline_response_200_3"
          examples:
            application/json:
              dappCount: 750
          headers: {}
      x-swagger-router-controller: "Statistics"
definitions:
  Announcement object: {}
  DApp collection: {}
  DApp Object: {}
  Tag collection: {}
  Statistics object: {}
  inline_response_200:
    required:
    - "message"
    - "status"
    - "url"
    - "urlText"
    properties:
      status:
        type: "boolean"
      message:
        type: "string"
      url:
        type: "string"
        description: "`null` if empty"
      urlText:
        type: "string"
        description: "`null` if empty"
  inline_response_200_1_badges:
    properties:
      complete:
        type: "string"
      token:
        type: "string"
      featured:
        type: "string"
      hot:
        type: "string"
    description: "returns an array, `[]` if empty"
  inline_response_200_1:
    required:
    - "additionalAuthors"
    - "author"
    - "badges"
    - "id"
    - "isNew"
    - "name"
    - "slug"
    - "status"
    - "teaser"
    properties:
      author:
        type: "string"
        description: "`null` if empty"
      additionalAuthors:
        type: "string"
        description: "`null` if empty"
      badges:
        $ref: "#/definitions/inline_response_200_1_badges"
      id:
        type: "number"
      isNew:
        type: "boolean"
      name:
        type: "string"
        description: "can't be null"
      slug:
        type: "string"
        description: "can't be null"
      status:
        type: "string"
        description: "`live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`,\
          \ `stealth`, `unknown`"
      teaser:
        type: "string"
        description: "can't be null"
  dapps_data_socials_facebook:
    required:
    - "url"
    properties:
      url:
        type: "string"
        description: "chars: max 255"
  dapps_data_socials_github:
    required:
    - "url"
    properties:
      url:
        type: "string"
        description: "chars: max 255"
    description: "chars: max 255"
  dapps_data_socials:
    properties:
      facebook:
        $ref: "#/definitions/dapps_data_socials_facebook"
      github:
        $ref: "#/definitions/dapps_data_socials_github"
      medium:
        $ref: "#/definitions/dapps_data_socials_facebook"
      other:
        $ref: "#/definitions/dapps_data_socials_facebook"
      reddit:
        $ref: "#/definitions/dapps_data_socials_facebook"
      slack:
        $ref: "#/definitions/dapps_data_socials_facebook"
      twitter:
        $ref: "#/definitions/dapps_data_socials_facebook"
      wiki:
        $ref: "#/definitions/dapps_data_socials_facebook"
  dapps_data:
    required:
    - "author"
    - "description"
    - "email"
    - "license"
    - "name"
    - "socials"
    - "status"
    - "teaser"
    - "website"
    properties:
      mainnet:
        type: "string"
      author:
        type: "string"
        description: "chars: min 3, max 25"
      additionalAuthors:
        type: "string"
        description: "chars: min 3, max 250"
      description:
        type: "string"
        description: "chars: min 3, max 1000"
      email:
        type: "string"
        description: "required `@` in string"
      license:
        type: "string"
        description: "chars: min 2, max 50"
      name:
        type: "string"
        description: "chars: min 3, max 25; unique"
      status:
        type: "string"
        description: "`live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`,\
          \ `stealth`, `unknown`"
      socials:
        $ref: "#/definitions/dapps_data_socials"
      teaser:
        type: "string"
        description: "chars: min 3, max 75"
      testnet:
        type: "string"
        description: "chars: max 255"
      website:
        type: "string"
        description: "chars: min 3, max 255"
  body:
    type: "object"
    required:
    - "data"
    properties:
      data:
        $ref: "#/definitions/dapps_data"
  inline_response_200_2_related:
    required:
    - "name"
    - "slug"
    - "status"
    - "teaser"
    properties:
      name:
        type: "string"
        description: "can't be empty"
      slug:
        type: "string"
        description: "can't be empty"
      status:
        type: "string"
        description: "`live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`,\
          \ `stealth`, `unknown`"
      teaser:
        type: "string"
        description: "can't be empty"
    description: "array, `[]` if empty"
  inline_response_200_2_socials_facebook:
    required:
    - "url"
    properties:
      url:
        type: "string"
        description: "can't be empty"
  inline_response_200_2_socials:
    properties:
      facebook:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      github:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      medium:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      other:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      reddit:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      slack:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
      twitter:
        $ref: "#/definitions/inline_response_200_2_socials_facebook"
    description: "`[]` if empty"
  inline_response_200_2:
    required:
    - "additionalAuthors"
    - "author"
    - "badges"
    - "created"
    - "description"
    - "isNew"
    - "lastUpdated"
    - "license"
    - "mainnet"
    - "name"
    - "related"
    - "ropsten"
    - "slug"
    - "socials"
    - "status"
    - "tags"
    - "teaser"
    - "website"
    properties:
      author:
        type: "string"
        description: "can't be empty"
      additionalAuthors:
        type: "string"
        description: "`null` if empty"
      badges:
        $ref: "#/definitions/inline_response_200_1_badges"
      created:
        type: "string"
        description: "can't be empty"
      description:
        type: "string"
        description: "can't be empty"
      isNew:
        type: "boolean"
      lastUpdated:
        type: "string"
        description: "can't be empty"
      license:
        type: "string"
        description: "can't be empty"
      mainnet:
        type: "string"
        description: "`null` if empty"
      name:
        type: "string"
        description: "can't be empty"
      related:
        $ref: "#/definitions/inline_response_200_2_related"
      ropsten:
        type: "string"
        description: "`null` if empty"
      slug:
        type: "string"
        description: "can't be empty"
      socials:
        $ref: "#/definitions/inline_response_200_2_socials"
      status:
        type: "string"
        description: "`live`, `wip`, `concept`, `prototype`, `demo`, `abandoned`,\
          \ `stealth`, `unknown`"
      tags:
        type: "object"
        description: "`[]` if empty"
        properties: {}
      teaser:
        type: "string"
        description: "can't be empty"
      website:
        type: "string"
        description: "can't be empty"
  inline_response_200_3:
    required:
    - "dappCount"
    properties:
      dappCount:
        type: "number"
