




var schema = new schema ({

      status  : { type : Boolean, required: true},
      message : { type : String, required: true },
      url     : { type : String, required: true /*, Description : `null` if empty */},
      urlText : { type : String, required: true /*, Description : `null` if empty */}

    });




/*
# Group Announcements

## Announcement object [/announcement]

The announcement area on the site is for a single announcement. It is retrieved only once, on initial page load. The site visitor then has the option to close the announcement for the remainder of the SPA usage. No cookies or localstorage are used, only site state.

### Retrieve Announcement [GET]

+ Response 200 (application/json)
    + Results: min (1), max (1)

    + Attributes
        + status (boolean, required)
        + message (string, required)
        + url (string, required) - `null` if empty
        + urlText (string, required) - `null` if empty

    + Body

            {
                "status": true,
                "message": "New website for State of the DApps",
                "url": "https://www.fazezero.com/",
                "urlText": "Check it out"
            }

+ Response 204
*/

