IncomingMessage {

  _readableState:
   ReadableState {
     objectMode: false,
     highWaterMark: 16384,
     buffer: BufferList { head: null, tail: null, length: 0 },
     length: 0,
     pipes: null,
     pipesCount: 0,
     flowing: null,
     ended: false,
     endEmitted: false,
     reading: false,
     sync: true,
     needReadable: false,
     emittedReadable: false,
     readableListening: false,
     resumeScheduled: false,
     destroyed: false,
     defaultEncoding: 'utf8',
     awaitDrain: 0,
     readingMore: true,
     decoder: null,
     encoding: null },
  readable: true,
  domain: null,
  _events: {},
  _eventsCount: 0,
  _maxListeners: undefined,
  socket:
   Socket {
     connecting: false,
     _hadError: false,
     _handle:
      TCP {
        bytesRead: 404,
        _externalStream: [External],
        fd: 18,
        reading: true,
        owner: [Object],
        onread: [Function: onread],
        onconnection: null,
        writeQueueSize: 0,
        _consumed: true },
     _parent: null,
     _host: null,
     _readableState:
      ReadableState {
        objectMode: false,
        highWaterMark: 16384,
        buffer: [Object],
        length: 0,
        pipes: null,
        pipesCount: 0,
        flowing: true,
        ended: false,
        endEmitted: false,
        reading: true,
        sync: false,
        needReadable: true,
        emittedReadable: false,
        readableListening: false,
        resumeScheduled: false,
        destroyed: false,
        defaultEncoding: 'utf8',
        awaitDrain: 0,
        readingMore: false,
        decoder: null,
        encoding: null },
     readable: true,
     domain: null,
     _events:
      { end: [Array],
        finish: [Function: onSocketFinish],
        _socketEnd: [Function: onSocketEnd],
        drain: [Array],
        timeout: [Function: socketOnTimeout],
        data: [Function: bound socketOnData],
        error: [Function: socketOnError],
        close: [Array],
        resume: [Function: onSocketResume],
        pause: [Function: onSocketPause] },
     _eventsCount: 10,
     _maxListeners: undefined,
     _writableState:
      WritableState {
        objectMode: false,
        highWaterMark: 16384,
        finalCalled: false,
        needDrain: false,
        ending: false,
        ended: false,
        finished: false,
        destroyed: false,
        decodeStrings: false,
        defaultEncoding: 'utf8',
        length: 0,
        writing: false,
        corked: 0,
        sync: true,
        bufferProcessing: false,
        onwrite: [Function: bound onwrite],
        writecb: null,
        writelen: 0,
        bufferedRequest: null,
        lastBufferedRequest: null,
        pendingcb: 0,
        prefinished: false,
        errorEmitted: false,
        bufferedRequestCount: 0,
        corkedRequestsFree: [Object] },
     writable: true,
     allowHalfOpen: true,
     _bytesDispatched: 0,
     _sockname: null,
     _pendingData: null,
     _pendingEncoding: '',
     server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _idleTimeout: 120000,
     _idleNext:
      Socket {
        connecting: false,
        _hadError: false,
        _handle: [Object],
        _parent: null,
        _host: null,
        _readableState: [Object],
        readable: true,
        domain: null,
        _events: [Object],
        _eventsCount: 10,
        _maxListeners: undefined,
        _writableState: [Object],
        writable: true,
        allowHalfOpen: true,
        _bytesDispatched: 0,
        _sockname: null,
        _pendingData: null,
        _pendingEncoding: '',
        server: [Object],
        _server: [Object],
        _idleTimeout: 120000,
        _idleNext: [Object],
        _idlePrev: [Object],
        _idleStart: 3363,
        _destroyed: false,
        parser: [Object],
        on: [Function: socketOnWrap],
        _paused: false,
        read: [Function],
        _consuming: true,
        [Symbol(asyncId)]: 24,
        [Symbol(bytesRead)]: 0,
        [Symbol(asyncId)]: 25,
        [Symbol(triggerAsyncId)]: 4 },
     _idlePrev:
      TimersList {
        _idleNext: [Object],
        _idlePrev: [Object],
        _timer: [Object],
        _unrefed: true,
        msecs: 120000,
        nextTick: false },
     _idleStart: 4636,
     _destroyed: false,
     parser:
      HTTPParser {
        '0': [Function: parserOnHeaders],
        '1': [Function: parserOnHeadersComplete],
        '2': [Function: parserOnBody],
        '3': [Function: parserOnMessageComplete],
        '4': [Function: bound onParserExecute],
        _headers: [],
        _url: '',
        _consumed: true,
        socket: [Object],
        incoming: [Object],
        outgoing: null,
        maxHeaderPairs: 2000,
        onIncoming: [Function: bound parserOnIncoming] },
     on: [Function: socketOnWrap],
     _paused: false,
     read: [Function],
     _consuming: true,
     _httpMessage:
      ServerResponse {
        domain: null,
        _events: [Object],
        _eventsCount: 1,
        _maxListeners: undefined,
        output: [],
        outputEncodings: [],
        outputCallbacks: [],
        outputSize: 0,
        writable: true,
        _last: false,
        upgrading: false,
        chunkedEncoding: false,
        shouldKeepAlive: true,
        useChunkedEncodingByDefault: true,
        sendDate: true,
        _removedConnection: false,
        _removedContLen: false,
        _removedTE: false,
        _contentLength: null,
        _hasBody: true,
        _trailer: '',
        finished: false,
        _headerSent: false,
        socket: [Object],
        connection: [Object],
        _header: null,
        _onPendingData: [Function: bound updateOutgoingData],
        _sent100: false,
        _expect_continue: false,
        [Symbol(outHeadersKey)]: [Object] },
     [Symbol(asyncId)]: 44,
     [Symbol(bytesRead)]: 0,
     [Symbol(asyncId)]: 45,
     [Symbol(triggerAsyncId)]: 4 },
  connection:
   Socket {
     connecting: false,
     _hadError: false,
     _handle:
      TCP {
        bytesRead: 404,
        _externalStream: [External],
        fd: 18,
        reading: true,
        owner: [Object],
        onread: [Function: onread],
        onconnection: null,
        writeQueueSize: 0,
        _consumed: true },
     _parent: null,
     _host: null,
     _readableState:
      ReadableState {
        objectMode: false,
        highWaterMark: 16384,
        buffer: [Object],
        length: 0,
        pipes: null,
        pipesCount: 0,
        flowing: true,
        ended: false,
        endEmitted: false,
        reading: true,
        sync: false,
        needReadable: true,
        emittedReadable: false,
        readableListening: false,
        resumeScheduled: false,
        destroyed: false,
        defaultEncoding: 'utf8',
        awaitDrain: 0,
        readingMore: false,
        decoder: null,
        encoding: null },
     readable: true,
     domain: null,
     _events:
      { end: [Array],
        finish: [Function: onSocketFinish],
        _socketEnd: [Function: onSocketEnd],
        drain: [Array],
        timeout: [Function: socketOnTimeout],
        data: [Function: bound socketOnData],
        error: [Function: socketOnError],
        close: [Array],
        resume: [Function: onSocketResume],
        pause: [Function: onSocketPause] },
     _eventsCount: 10,
     _maxListeners: undefined,
     _writableState:
      WritableState {
        objectMode: false,
        highWaterMark: 16384,
        finalCalled: false,
        needDrain: false,
        ending: false,
        ended: false,
        finished: false,
        destroyed: false,
        decodeStrings: false,
        defaultEncoding: 'utf8',
        length: 0,
        writing: false,
        corked: 0,
        sync: true,
        bufferProcessing: false,
        onwrite: [Function: bound onwrite],
        writecb: null,
        writelen: 0,
        bufferedRequest: null,
        lastBufferedRequest: null,
        pendingcb: 0,
        prefinished: false,
        errorEmitted: false,
        bufferedRequestCount: 0,
        corkedRequestsFree: [Object] },
     writable: true,
     allowHalfOpen: true,
     _bytesDispatched: 0,
     _sockname: null,
     _pendingData: null,
     _pendingEncoding: '',
     server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _idleTimeout: 120000,
     _idleNext:
      Socket {
        connecting: false,
        _hadError: false,
        _handle: [Object],
        _parent: null,
        _host: null,
        _readableState: [Object],
        readable: true,
        domain: null,
        _events: [Object],
        _eventsCount: 10,
        _maxListeners: undefined,
        _writableState: [Object],
        writable: true,
        allowHalfOpen: true,
        _bytesDispatched: 0,
        _sockname: null,
        _pendingData: null,
        _pendingEncoding: '',
        server: [Object],
        _server: [Object],
        _idleTimeout: 120000,
        _idleNext: [Object],
        _idlePrev: [Object],
        _idleStart: 3363,
        _destroyed: false,
        parser: [Object],
        on: [Function: socketOnWrap],
        _paused: false,
        read: [Function],
        _consuming: true,
        [Symbol(asyncId)]: 24,
        [Symbol(bytesRead)]: 0,
        [Symbol(asyncId)]: 25,
        [Symbol(triggerAsyncId)]: 4 },
     _idlePrev:
      TimersList {
        _idleNext: [Object],
        _idlePrev: [Object],
        _timer: [Object],
        _unrefed: true,
        msecs: 120000,
        nextTick: false },
     _idleStart: 4636,
     _destroyed: false,
     parser:
      HTTPParser {
        '0': [Function: parserOnHeaders],
        '1': [Function: parserOnHeadersComplete],
        '2': [Function: parserOnBody],
        '3': [Function: parserOnMessageComplete],
        '4': [Function: bound onParserExecute],
        _headers: [],
        _url: '',
        _consumed: true,
        socket: [Object],
        incoming: [Object],
        outgoing: null,
        maxHeaderPairs: 2000,
        onIncoming: [Function: bound parserOnIncoming] },
     on: [Function: socketOnWrap],
     _paused: false,
     read: [Function],
     _consuming: true,
     _httpMessage:
      ServerResponse {
        domain: null,
        _events: [Object],
        _eventsCount: 1,
        _maxListeners: undefined,
        output: [],
        outputEncodings: [],
        outputCallbacks: [],
        outputSize: 0,
        writable: true,
        _last: false,
        upgrading: false,
        chunkedEncoding: false,
        shouldKeepAlive: true,
        useChunkedEncodingByDefault: true,
        sendDate: true,
        _removedConnection: false,
        _removedContLen: false,
        _removedTE: false,
        _contentLength: null,
        _hasBody: true,
        _trailer: '',
        finished: false,
        _headerSent: false,
        socket: [Object],
        connection: [Object],
        _header: null,
        _onPendingData: [Function: bound updateOutgoingData],
        _sent100: false,
        _expect_continue: false,
        [Symbol(outHeadersKey)]: [Object] },
     [Symbol(asyncId)]: 44,
     [Symbol(bytesRead)]: 0,
     [Symbol(asyncId)]: 45,
     [Symbol(triggerAsyncId)]: 4 },
  httpVersionMajor: 1,
  httpVersionMinor: 1,
  httpVersion: '1.1',
  complete: false,
  headers:
   { host: 'localhost:8080',
     connection: 'keep-alive',
     accept: 'application/json, text/plain, */*',
     origin: 'http://localhost:3000',
     'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
     referer: 'http://localhost:3000/',
     'accept-encoding': 'gzip, deflate, br',
     'accept-language': 'en-US,en;q=0.9,ar;q=0.8,fr;q=0.7' },
  rawHeaders:
   [ 'Host',
     'localhost:8080',
     'Connection',
     'keep-alive',
     'Accept',
     'application/json, text/plain, */*',
     'Origin',
     'http://localhost:3000',
     'User-Agent',
     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
     'Referer',
     'http://localhost:3000/',
     'Accept-Encoding',
     'gzip, deflate, br',
     'Accept-Language',
     'en-US,en;q=0.9,ar;q=0.8,fr;q=0.7' ],
  trailers: {},
  rawTrailers: [],
  upgrade: false,
  url: '/announcement',
  method: 'GET',
  statusCode: null,
  statusMessage: null,
  client:
   Socket {
     connecting: false,
     _hadError: false,
     _handle:
      TCP {
        bytesRead: 404,
        _externalStream: [External],
        fd: 18,
        reading: true,
        owner: [Object],
        onread: [Function: onread],
        onconnection: null,
        writeQueueSize: 0,
        _consumed: true },
     _parent: null,
     _host: null,
     _readableState:
      ReadableState {
        objectMode: false,
        highWaterMark: 16384,
        buffer: [Object],
        length: 0,
        pipes: null,
        pipesCount: 0,
        flowing: true,
        ended: false,
        endEmitted: false,
        reading: true,
        sync: false,
        needReadable: true,
        emittedReadable: false,
        readableListening: false,
        resumeScheduled: false,
        destroyed: false,
        defaultEncoding: 'utf8',
        awaitDrain: 0,
        readingMore: false,
        decoder: null,
        encoding: null },
     readable: true,
     domain: null,
     _events:
      { end: [Array],
        finish: [Function: onSocketFinish],
        _socketEnd: [Function: onSocketEnd],
        drain: [Array],
        timeout: [Function: socketOnTimeout],
        data: [Function: bound socketOnData],
        error: [Function: socketOnError],
        close: [Array],
        resume: [Function: onSocketResume],
        pause: [Function: onSocketPause] },
     _eventsCount: 10,
     _maxListeners: undefined,
     _writableState:
      WritableState {
        objectMode: false,
        highWaterMark: 16384,
        finalCalled: false,
        needDrain: false,
        ending: false,
        ended: false,
        finished: false,
        destroyed: false,
        decodeStrings: false,
        defaultEncoding: 'utf8',
        length: 0,
        writing: false,
        corked: 0,
        sync: true,
        bufferProcessing: false,
        onwrite: [Function: bound onwrite],
        writecb: null,
        writelen: 0,
        bufferedRequest: null,
        lastBufferedRequest: null,
        pendingcb: 0,
        prefinished: false,
        errorEmitted: false,
        bufferedRequestCount: 0,
        corkedRequestsFree: [Object] },
     writable: true,
     allowHalfOpen: true,
     _bytesDispatched: 0,
     _sockname: null,
     _pendingData: null,
     _pendingEncoding: '',
     server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _server:
      Server {
        domain: null,
        _events: [Object],
        _eventsCount: 2,
        _maxListeners: undefined,
        _connections: 5,
        _handle: [Object],
        _usingSlaves: false,
        _slaves: [],
        _unref: false,
        allowHalfOpen: true,
        pauseOnConnect: false,
        httpAllowHalfOpen: false,
        timeout: 120000,
        keepAliveTimeout: 5000,
        _pendingResponseData: 0,
        maxHeadersCount: null,
        _connectionKey: '6::::8080',
        [Symbol(asyncId)]: 4 },
     _idleTimeout: 120000,
     _idleNext:
      Socket {
        connecting: false,
        _hadError: false,
        _handle: [Object],
        _parent: null,
        _host: null,
        _readableState: [Object],
        readable: true,
        domain: null,
        _events: [Object],
        _eventsCount: 10,
        _maxListeners: undefined,
        _writableState: [Object],
        writable: true,
        allowHalfOpen: true,
        _bytesDispatched: 0,
        _sockname: null,
        _pendingData: null,
        _pendingEncoding: '',
        server: [Object],
        _server: [Object],
        _idleTimeout: 120000,
        _idleNext: [Object],
        _idlePrev: [Object],
        _idleStart: 3363,
        _destroyed: false,
        parser: [Object],
        on: [Function: socketOnWrap],
        _paused: false,
        read: [Function],
        _consuming: true,
        [Symbol(asyncId)]: 24,
        [Symbol(bytesRead)]: 0,
        [Symbol(asyncId)]: 25,
        [Symbol(triggerAsyncId)]: 4 },
     _idlePrev:
      TimersList {
        _idleNext: [Object],
        _idlePrev: [Object],
        _timer: [Object],
        _unrefed: true,
        msecs: 120000,
        nextTick: false },
     _idleStart: 4636,
     _destroyed: false,
     parser:
      HTTPParser {
        '0': [Function: parserOnHeaders],
        '1': [Function: parserOnHeadersComplete],
        '2': [Function: parserOnBody],
        '3': [Function: parserOnMessageComplete],
        '4': [Function: bound onParserExecute],
        _headers: [],
        _url: '',
        _consumed: true,
        socket: [Object],
        incoming: [Object],
        outgoing: null,
        maxHeaderPairs: 2000,
        onIncoming: [Function: bound parserOnIncoming] },
     on: [Function: socketOnWrap],
     _paused: false,
     read: [Function],
     _consuming: true,
     _httpMessage:
      ServerResponse {
        domain: null,
        _events: [Object],
        _eventsCount: 1,
        _maxListeners: undefined,
        output: [],
        outputEncodings: [],
        outputCallbacks: [],
        outputSize: 0,
        writable: true,
        _last: false,
        upgrading: false,
        chunkedEncoding: false,
        shouldKeepAlive: true,
        useChunkedEncodingByDefault: true,
        sendDate: true,
        _removedConnection: false,
        _removedContLen: false,
        _removedTE: false,
        _contentLength: null,
        _hasBody: true,
        _trailer: '',
        finished: false,
        _headerSent: false,
        socket: [Object],
        connection: [Object],
        _header: null,
        _onPendingData: [Function: bound updateOutgoingData],
        _sent100: false,
        _expect_continue: false,
        [Symbol(outHeadersKey)]: [Object] },
     [Symbol(asyncId)]: 44,
     [Symbol(bytesRead)]: 0,
     [Symbol(asyncId)]: 45,
     [Symbol(triggerAsyncId)]: 4 },
  _consuming: false,
  _dumped: false,
  originalUrl: '/announcement',
  _parsedUrl:
   Url {
     protocol: null,
     slashes: null,
     auth: null,
     host: null,
     port: null,
     hostname: null,
     hash: null,
     search: null,
     query: null,
     pathname: '/announcement',
     path: '/announcement',
     href: '/announcement',
     _raw: '/announcement' },
  swagger:
   { apiPath: '/announcement',
     path: { get: [Object] },
     params: {},
     swaggerObject:
      { swagger: '2.0',
        info: [Object],
        paths: [Object],
        definitions: [Object] },
     operation:
      { tags: [Array],
        summary: 'Retrieve Announcement',
        description: '',
        operationId: 'announcementGET',
        produces: [Array],
        parameters: [],
        responses: [Object],
        'x-swagger-router-controller': 'Announcements' },
     operationPath: [ 'paths', '/announcement', 'get' ],
     operationParameters: [],
     security: [],
     swaggerVersion: '2.0',
     useStubs: false } }





     ServerResponse {
      domain: null,
      _events: { finish: [Function: bound resOnFinish] },
      _eventsCount: 1,
      _maxListeners: undefined,
      output: [],
      outputEncodings: [],
      outputCallbacks: [],
      outputSize: 0,
      writable: true,
      _last: false,
      upgrading: false,
      chunkedEncoding: false,
      shouldKeepAlive: true,
      useChunkedEncodingByDefault: true,
      sendDate: true,
      _removedConnection: false,
      _removedContLen: false,
      _removedTE: false,
      _contentLength: null,
      _hasBody: true,
      _trailer: '',
      finished: false,
      _headerSent: false,
      socket:
       Socket {
         connecting: false,
         _hadError: false,
         _handle:
          TCP {
            bytesRead: 404,
            _externalStream: [External],
            fd: 18,
            reading: true,
            owner: [Object],
            onread: [Function: onread],
            onconnection: null,
            writeQueueSize: 0,
            _consumed: true },
         _parent: null,
         _host: null,
         _readableState:
          ReadableState {
            objectMode: false,
            highWaterMark: 16384,
            buffer: [Object],
            length: 0,
            pipes: null,
            pipesCount: 0,
            flowing: true,
            ended: false,
            endEmitted: false,
            reading: true,
            sync: false,
            needReadable: true,
            emittedReadable: false,
            readableListening: false,
            resumeScheduled: false,
            destroyed: false,
            defaultEncoding: 'utf8',
            awaitDrain: 0,
            readingMore: false,
            decoder: null,
            encoding: null },
         readable: true,
         domain: null,
         _events:
          { end: [Array],
            finish: [Function: onSocketFinish],
            _socketEnd: [Function: onSocketEnd],
            drain: [Array],
            timeout: [Function: socketOnTimeout],
            data: [Function: bound socketOnData],
            error: [Function: socketOnError],
            close: [Array],
            resume: [Function: onSocketResume],
            pause: [Function: onSocketPause] },
         _eventsCount: 10,
         _maxListeners: undefined,
         _writableState:
          WritableState {
            objectMode: false,
            highWaterMark: 16384,
            finalCalled: false,
            needDrain: false,
            ending: false,
            ended: false,
            finished: false,
            destroyed: false,
            decodeStrings: false,
            defaultEncoding: 'utf8',
            length: 0,
            writing: false,
            corked: 0,
            sync: true,
            bufferProcessing: false,
            onwrite: [Function: bound onwrite],
            writecb: null,
            writelen: 0,
            bufferedRequest: null,
            lastBufferedRequest: null,
            pendingcb: 0,
            prefinished: false,
            errorEmitted: false,
            bufferedRequestCount: 0,
            corkedRequestsFree: [Object] },
         writable: true,
         allowHalfOpen: true,
         _bytesDispatched: 0,
         _sockname: null,
         _pendingData: null,
         _pendingEncoding: '',
         server:
          Server {
            domain: null,
            _events: [Object],
            _eventsCount: 2,
            _maxListeners: undefined,
            _connections: 5,
            _handle: [Object],
            _usingSlaves: false,
            _slaves: [],
            _unref: false,
            allowHalfOpen: true,
            pauseOnConnect: false,
            httpAllowHalfOpen: false,
            timeout: 120000,
            keepAliveTimeout: 5000,
            _pendingResponseData: 0,
            maxHeadersCount: null,
            _connectionKey: '6::::8080',
            [Symbol(asyncId)]: 4 },
         _server:
          Server {
            domain: null,
            _events: [Object],
            _eventsCount: 2,
            _maxListeners: undefined,
            _connections: 5,
            _handle: [Object],
            _usingSlaves: false,
            _slaves: [],
            _unref: false,
            allowHalfOpen: true,
            pauseOnConnect: false,
            httpAllowHalfOpen: false,
            timeout: 120000,
            keepAliveTimeout: 5000,
            _pendingResponseData: 0,
            maxHeadersCount: null,
            _connectionKey: '6::::8080',
            [Symbol(asyncId)]: 4 },
         _idleTimeout: 120000,
         _idleNext:
          Socket {
            connecting: false,
            _hadError: false,
            _handle: [Object],
            _parent: null,
            _host: null,
            _readableState: [Object],
            readable: true,
            domain: null,
            _events: [Object],
            _eventsCount: 10,
            _maxListeners: undefined,
            _writableState: [Object],
            writable: true,
            allowHalfOpen: true,
            _bytesDispatched: 0,
            _sockname: null,
            _pendingData: null,
            _pendingEncoding: '',
            server: [Object],
            _server: [Object],
            _idleTimeout: 120000,
            _idleNext: [Object],
            _idlePrev: [Object],
            _idleStart: 3090,
            _destroyed: false,
            parser: [Object],
            on: [Function: socketOnWrap],
            _paused: false,
            read: [Function],
            _consuming: true,
            [Symbol(asyncId)]: 24,
            [Symbol(bytesRead)]: 0,
            [Symbol(asyncId)]: 25,
            [Symbol(triggerAsyncId)]: 4 },
         _idlePrev:
          TimersList {
            _idleNext: [Object],
            _idlePrev: [Object],
            _timer: [Object],
            _unrefed: true,
            msecs: 120000,
            nextTick: false },
         _idleStart: 4939,
         _destroyed: false,
         parser:
          HTTPParser {
            '0': [Function: parserOnHeaders],
            '1': [Function: parserOnHeadersComplete],
            '2': [Function: parserOnBody],
            '3': [Function: parserOnMessageComplete],
            '4': [Function: bound onParserExecute],
            _headers: [],
            _url: '',
            _consumed: true,
            socket: [Object],
            incoming: [Object],
            outgoing: null,
            maxHeaderPairs: 2000,
            onIncoming: [Function: bound parserOnIncoming] },
         on: [Function: socketOnWrap],
         _paused: false,
         read: [Function],
         _consuming: true,
         _httpMessage: [Circular],
         [Symbol(asyncId)]: 44,
         [Symbol(bytesRead)]: 0,
         [Symbol(asyncId)]: 45,
         [Symbol(triggerAsyncId)]: 4 },
      connection:
       Socket {
         connecting: false,
         _hadError: false,
         _handle:
          TCP {
            bytesRead: 404,
            _externalStream: [External],
            fd: 18,
            reading: true,
            owner: [Object],
            onread: [Function: onread],
            onconnection: null,
            writeQueueSize: 0,
            _consumed: true },
         _parent: null,
         _host: null,
         _readableState:
          ReadableState {
            objectMode: false,
            highWaterMark: 16384,
            buffer: [Object],
            length: 0,
            pipes: null,
            pipesCount: 0,
            flowing: true,
            ended: false,
            endEmitted: false,
            reading: true,
            sync: false,
            needReadable: true,
            emittedReadable: false,
            readableListening: false,
            resumeScheduled: false,
            destroyed: false,
            defaultEncoding: 'utf8',
            awaitDrain: 0,
            readingMore: false,
            decoder: null,
            encoding: null },
         readable: true,
         domain: null,
         _events:
          { end: [Array],
            finish: [Function: onSocketFinish],
            _socketEnd: [Function: onSocketEnd],
            drain: [Array],
            timeout: [Function: socketOnTimeout],
            data: [Function: bound socketOnData],
            error: [Function: socketOnError],
            close: [Array],
            resume: [Function: onSocketResume],
            pause: [Function: onSocketPause] },
         _eventsCount: 10,
         _maxListeners: undefined,
         _writableState:
          WritableState {
            objectMode: false,
            highWaterMark: 16384,
            finalCalled: false,
            needDrain: false,
            ending: false,
            ended: false,
            finished: false,
            destroyed: false,
            decodeStrings: false,
            defaultEncoding: 'utf8',
            length: 0,
            writing: false,
            corked: 0,
            sync: true,
            bufferProcessing: false,
            onwrite: [Function: bound onwrite],
            writecb: null,
            writelen: 0,
            bufferedRequest: null,
            lastBufferedRequest: null,
            pendingcb: 0,
            prefinished: false,
            errorEmitted: false,
            bufferedRequestCount: 0,
            corkedRequestsFree: [Object] },
         writable: true,
         allowHalfOpen: true,
         _bytesDispatched: 0,
         _sockname: null,
         _pendingData: null,
         _pendingEncoding: '',
         server:
          Server {
            domain: null,
            _events: [Object],
            _eventsCount: 2,
            _maxListeners: undefined,
            _connections: 5,
            _handle: [Object],
            _usingSlaves: false,
            _slaves: [],
            _unref: false,
            allowHalfOpen: true,
            pauseOnConnect: false,
            httpAllowHalfOpen: false,
            timeout: 120000,
            keepAliveTimeout: 5000,
            _pendingResponseData: 0,
            maxHeadersCount: null,
            _connectionKey: '6::::8080',
            [Symbol(asyncId)]: 4 },
         _server:
          Server {
            domain: null,
            _events: [Object],
            _eventsCount: 2,
            _maxListeners: undefined,
            _connections: 5,
            _handle: [Object],
            _usingSlaves: false,
            _slaves: [],
            _unref: false,
            allowHalfOpen: true,
            pauseOnConnect: false,
            httpAllowHalfOpen: false,
            timeout: 120000,
            keepAliveTimeout: 5000,
            _pendingResponseData: 0,
            maxHeadersCount: null,
            _connectionKey: '6::::8080',
            [Symbol(asyncId)]: 4 },
         _idleTimeout: 120000,
         _idleNext:
          Socket {
            connecting: false,
            _hadError: false,
            _handle: [Object],
            _parent: null,
            _host: null,
            _readableState: [Object],
            readable: true,
            domain: null,
            _events: [Object],
            _eventsCount: 10,
            _maxListeners: undefined,
            _writableState: [Object],
            writable: true,
            allowHalfOpen: true,
            _bytesDispatched: 0,
            _sockname: null,
            _pendingData: null,
            _pendingEncoding: '',
            server: [Object],
            _server: [Object],
            _idleTimeout: 120000,
            _idleNext: [Object],
            _idlePrev: [Object],
            _idleStart: 3090,
            _destroyed: false,
            parser: [Object],
            on: [Function: socketOnWrap],
            _paused: false,
            read: [Function],
            _consuming: true,
            [Symbol(asyncId)]: 24,
            [Symbol(bytesRead)]: 0,
            [Symbol(asyncId)]: 25,
            [Symbol(triggerAsyncId)]: 4 },
         _idlePrev:
          TimersList {
            _idleNext: [Object],
            _idlePrev: [Object],
            _timer: [Object],
            _unrefed: true,
            msecs: 120000,
            nextTick: false },
         _idleStart: 4939,
         _destroyed: false,
         parser:
          HTTPParser {
            '0': [Function: parserOnHeaders],
            '1': [Function: parserOnHeadersComplete],
            '2': [Function: parserOnBody],
            '3': [Function: parserOnMessageComplete],
            '4': [Function: bound onParserExecute],
            _headers: [],
            _url: '',
            _consumed: true,
            socket: [Object],
            incoming: [Object],
            outgoing: null,
            maxHeaderPairs: 2000,
            onIncoming: [Function: bound parserOnIncoming] },
         on: [Function: socketOnWrap],
         _paused: false,
         read: [Function],
         _consuming: true,
         _httpMessage: [Circular],
         [Symbol(asyncId)]: 44,
         [Symbol(bytesRead)]: 0,
         [Symbol(asyncId)]: 45,
         [Symbol(triggerAsyncId)]: 4 },
      _header: null,
      _onPendingData: [Function: bound updateOutgoingData],
      _sent100: false,
      _expect_continue: false,
      [Symbol(outHeadersKey)]:
       { 'access-control-allow-origin': [ 'Access-Control-Allow-Origin', '*' ],
         'access-control-allow-headers':
          [ 'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept' ],
         'access-control-allow-methods':
          [ 'Access-Control-Allow-Methods',
            'POST, GET, PATCH, DELETE, OPTIONS' ] } }
